export default function filterByName(node: Element, name: string, exactMatch?: boolean): boolean;
