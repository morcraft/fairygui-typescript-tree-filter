"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var byName_1 = require("./byName");
var byText_1 = require("./byText");
var byType_1 = require("./byType");
var filters = {
    byName: byName_1.default,
    byText: byText_1.default,
    byType: byType_1.default
};
exports.default = filters;
