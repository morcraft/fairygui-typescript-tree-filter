export default function filterByText(node: Element, text: string, exactMatch?: boolean): boolean;
