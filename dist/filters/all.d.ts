import filterByName from "./byName";
import filterByText from "./byText";
import filterByType from "./byType";
declare const filters: {
    byName: typeof filterByName;
    byText: typeof filterByText;
    byType: typeof filterByType;
};
export default filters;
export declare type Filter = keyof typeof filters;
