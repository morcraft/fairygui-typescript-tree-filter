"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function filterByName(node, name, exactMatch) {
    var _name = node.getAttribute('name');
    if (typeof _name != 'string')
        return false;
    if (!exactMatch)
        return _name.toLowerCase().indexOf(name.toLowerCase()) > -1;
    return _name.toLowerCase() == name.toLowerCase();
}
exports.default = filterByName;
