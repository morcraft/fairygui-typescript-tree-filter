export default function filterByType(node: Element, type: string, exactMatch?: boolean): boolean;
