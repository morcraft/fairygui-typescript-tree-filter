"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function filterByText(node, text, exactMatch) {
    var _text = node.getAttribute('text');
    if (typeof _text != 'string')
        return false;
    if (!exactMatch)
        return _text.toLowerCase().indexOf(text.toLowerCase()) > -1;
    return _text == text;
}
exports.default = filterByText;
