"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function filterByType(node, type, exactMatch) {
    var tag = node.nodeName.toLowerCase();
    if (typeof tag != 'string')
        return false;
    if (!exactMatch)
        return tag.toLowerCase().indexOf(type.toLowerCase()) > -1;
    return tag.toLowerCase() == type.toLowerCase();
}
exports.default = filterByType;
