import TreeGenerator from 'fairygui-typescript-tree-generator';
import { TreeNode } from 'fairygui-typescript-tree-generator/dist/tree';
import { Filter } from './filters/all';
export interface XMLNode {
    [s: string]: TreeNode;
}
export default function getNodes(treeGenerator: TreeGenerator, treeNode: TreeNode, path: string[], separatorCharacter: string, filterType: Filter, filterCriterion: string, exactMatch?: boolean): XMLNode[];
