"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var all_1 = require("./filters/all");
function getNodes(treeGenerator, treeNode, path, separatorCharacter, filterType, filterCriterion, exactMatch) {
    var treeNodes = [];
    if (!treeNode || !Array.isArray(treeNode._children))
        return treeNodes;
    var nodeCollection = treeGenerator.elementsTree.nodeCollection;
    treeNode._children.forEach((function (element) {
        var _a;
        var id = element.node.getAttribute('id');
        if (!id || !id.length)
            throw new Error("Invalid id for node with name " + element.nodeName);
        var _path = path.concat(id);
        if (typeof all_1.default[filterType] != 'function')
            throw new Error("Invalid filter type " + filterType);
        if (all_1.default[filterType](element.node, filterCriterion, exactMatch)) {
            treeNodes.push((_a = {},
                _a[_path.join(separatorCharacter)] = element,
                _a));
        }
        var nodeInTree = nodeCollection[element.nodeName];
        var inheritedComponent = nodeCollection[nodeInTree.inherits];
        if (!inheritedComponent)
            return true; //could omit this, but kept it to reduce number of callbacks
        var _treeNodes = getNodes(treeGenerator, inheritedComponent, _path, separatorCharacter, filterType, filterCriterion, exactMatch);
        _treeNodes.forEach((function (text) {
            treeNodes.push(text);
        }));
    }));
    return treeNodes;
}
exports.default = getNodes;
