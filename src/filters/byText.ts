export default function filterByText(node: Element, text: string, exactMatch?: boolean){
    const _text = node.getAttribute('text')
    if(typeof _text != 'string') return false
    if(!exactMatch)
        return _text.toLowerCase().indexOf(text.toLowerCase()) > -1
    
    return _text == text
}