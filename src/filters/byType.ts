export default function filterByType(node: Element, type: string, exactMatch?: boolean){
    const tag = node.nodeName.toLowerCase()
    if(typeof tag != 'string') return false
    if(!exactMatch)
        return tag.toLowerCase().indexOf(type.toLowerCase()) > -1
    
    return tag.toLowerCase() == type.toLowerCase()
}