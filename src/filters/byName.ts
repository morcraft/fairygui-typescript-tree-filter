export default function filterByName(node: Element, name: string, exactMatch?: boolean){
    const _name = node.getAttribute('name')
    if(typeof _name != 'string') return false
    if(!exactMatch)
        return _name.toLowerCase().indexOf(name.toLowerCase()) > -1

    return _name.toLowerCase() == name.toLowerCase()
}