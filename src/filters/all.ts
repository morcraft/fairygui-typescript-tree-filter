import filterByName from "./byName"
import filterByText from "./byText";
import filterByType from "./byType";

const filters = {
    byName: filterByName,
    byText: filterByText,
    byType: filterByType
}

export default filters

export type Filter = keyof typeof filters