import TreeGenerator from 'fairygui-typescript-tree-generator'
import { TreeNode } from 'fairygui-typescript-tree-generator/dist/tree'
import Filters, { Filter } from './filters/all'

export interface XMLNode{
    [s: string]: TreeNode
} 

export default function getNodes(treeGenerator: TreeGenerator, treeNode: TreeNode, path: string[], separatorCharacter: string, filterType: Filter, filterCriterion: string, exactMatch?: boolean){
    const treeNodes: XMLNode[] = []

    if(!treeNode || !Array.isArray(treeNode._children)) return treeNodes

    const nodeCollection = treeGenerator.elementsTree.nodeCollection

    treeNode._children.forEach((element => {
        const id = element.node.getAttribute('id')
        if(!id || !id.length)
            throw new Error(`Invalid id for node with name ${element.nodeName}`)

        const _path = path.concat(id)

        if(typeof Filters[filterType] != 'function')
            throw new Error(`Invalid filter type ${filterType}`)

        if(Filters[filterType](element.node, filterCriterion, exactMatch)){
            treeNodes.push({
                [_path.join(separatorCharacter)]: element
            })
        }

        const nodeInTree = nodeCollection[element.nodeName]
        const inheritedComponent = nodeCollection[nodeInTree.inherits]
        if(!inheritedComponent) return true //could omit this, but kept it to reduce number of callbacks
        const _treeNodes = getNodes(treeGenerator, inheritedComponent, _path, separatorCharacter, filterType, filterCriterion, exactMatch)
        _treeNodes.forEach((text => {
            treeNodes.push(text)
        }))

    }))

    return treeNodes
}